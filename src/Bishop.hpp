//
//  Bishop.hpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#ifndef Bishop_hpp
#define Bishop_hpp

#include "Piece.hpp"

class Bishop: public Piece{
    
public:
    Bishop(ofVec3f p, bool c);
    bool legal (ofVec3f p);
    
};

#endif /* Bishop_hpp */
