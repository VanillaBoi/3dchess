//
//  Queen.hpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#ifndef Queen_hpp
#define Queen_hpp


#include "Piece.hpp"

class Queen : public Piece{
    
public:
    Queen(ofVec3f p, bool c);
    bool legal(ofVec3f p);
    
    
};


#endif /* Queen_hpp */
