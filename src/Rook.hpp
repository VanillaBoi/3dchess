//
//  Rook.hpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#ifndef Rook_hpp
#define Rook_hpp

#include "Piece.hpp"

class Rook : public Piece{
    
public:
    Rook(ofVec3f p, bool c);
    bool legal (ofVec3f p);
    
    
};



#endif /* Rook_hpp */
