//
//  Piece.cpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#include "Piece.hpp"


Piece::Piece(ofVec3f p, bool c)
{
     alive=1;
     position=p;
     model.setPosition(p.x*150,p.y*150,p.z*150);
     model.setScale(0.1, 0.1, 0.1);
    
    color=c;
}

void Piece::draw()
{
    
    if(selected)//display squares the piece can move to
    {
        std::cout<<legal(position)<<std::endl;
        ofFill();
        ofSetColor(255,255, 0, 90);
        for(int x=0;x<4;x++)
        {
            for(int y=0;y<4;y++)
            {
                for(int z=0;z<4;z++)
                {
                    if(legal(ofVec3f(x,y,z)))
                    {
                        bool friendly=false;
                        for(int i=0;i<o.size();i++)//cannot move to a square where a friendly piece is sitting
                        {
                            if( o[i]->position == ofVec3f(x,y,z) && o[i]->color == color)
                            {
                                friendly=true;
                            }
                        }
                        if(!friendly)
                        {
                            ofDrawBox(x*150,y*150,z*150,50);
                        }
                    }
                }
            }
        }
    }
    
    
    if(color==0)
        ofSetColor(255);
    else
        ofSetColor(0);
    
    model.drawWireframe();
    
    if(color==0)
        ofSetColor(0,0,0);
    else
        ofSetColor(255);
    
    model.drawFaces();
  
}

bool Piece::move(ofVec3f p)
{
    
    bool moved = false;//return wether piece has moved
    
    for(int i=0;i<o.size();i++)//cannot move to a square where a friendly piece is sitting
    {
        if( o[i]->position == p && o[i]->color == color)
        {
            return false;
        }
    }
    
    if( legal(p)==true)
    {
        for(int i=0;i<o.size();i++)
        {
            if( o[i]->position == p && o[i]->color != color)
            {
                o[i]->alive=0;
                o[i] = nullptr;
                o.erase(std::remove(o.begin(), o.end(), nullptr), o.end());
                break;
            }
        }
       
        position=p;
        model.setPosition(p.x*150,p.y*150,p.z*150);
        moved=true;
    }
    return moved;
}

bool Piece::legal(ofVec3f p)
{
   
}

bool Piece::isKing()
{
    return false;
}

int Piece::numLegal()
{
    return -1;
}

