//
//  Knight.cpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#include "Knight.hpp"


Knight::Knight(ofVec3f p, bool c) : Piece(p,c)
{
     model.loadModel("Knight.3ds");
}

bool Knight::legal(ofVec3f p)
{
    const int offsets[24][3]={
    {1,-2,0},
    {2,-1,0},
    {2,1,0},
    {1,2,0},
//
    {-1,2,0},
    {-2,1,0},
    {-2,-1,0},
    {-1,-2,0},
//
    {0,1,2},
    {0,1,-2},
    {0,-1,-2},
    {0,-1,2},
//
    {0,2,1},
    {0,-2,-1},
    {0,2,-1},
    {0,-2,1},
//
    {2,0,1},
    {-2,0,-1},
    {-2,0,1},
    {2,0,-1},
//
    {1,0,2},
    {-1,0,-2},
    {-1,0,2},
    {1,0,-2}
};
    
for (int i=0;i<24;i++)
    {
        if(p.x == position.x+offsets[i][0] && p.y==position.y+offsets[i][1] && p.z==position.z+offsets[i][2] )
        {
            return true;
        }
    
    }
    return false;
}
