//
//  Board.cpp
//  3DChess
//
//  Created by Marlon Youngerman on 27/12/2018.
//

#include "Board.hpp"



Board::Board()
{
   
}

void Board::update(){
    
}


void Board::draw()
{
    //ofNoFill();
    ofSetLineWidth(1);

    for(int x=0;x<4;x++)
    {
        for(int y=0;y<4;y++)
        {
            for(int z=0;z<4;z++)
            {
                
                if( (x+y+z)%2==0 )
                {
                    ofSetColor(0);
                    ofFill();
                    ofDrawBox(x*150,y*150,z*150,50);
//                    ofNoFill();
//                    ofSetColor(255, 255, 255);
//                    ofDrawBox(x*150,y*150,z*150,50);

                    
                }
                else
                {
                    ofSetColor(255);
                    ofFill();
                    ofDrawBox(x*150,y*150,z*150,50);
//                    ofNoFill();
//                    ofSetColor(0);
//                    ofDrawBox(x*150,y*150,z*150,50);
                    
                }
                
                
            }
        }
    }
}

