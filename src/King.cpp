//
//  King.cpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#include "King.hpp"


King::King(ofVec3f p, bool c) : Piece(p,c)
{
    model.loadModel("King.3ds");
}

bool King::legal(ofVec3f p)
{
   if(p==position)
       return false;
    
    //if p!=any other teams's piece legal move, also if King's num legal moves==0 loose game
    
     if( ( ofDist(p.x,p.y,p.z,position.x,position.y,position.z)<2)  )
        {
            for(int i=0;i<o.size();i++)
            {
                if(!(o[i]->isKing()))
                {
                    if(o[i]->color != color && o[i]->legal(p))
                    {
                        return false;
                    }
                }
            }
        return true;
        
    }
    else
    {
        return false;
    }
}

int King::numLegal()
{
   int num=0;
    for(int x=0;x<4;x++)
    {
        for(int y=0;y<4;y++)
        {
            for(int z=0;z<4;z++)
            {
                if(legal(ofVec3f(x,y,z)))
                {
                    num++;
                }
            }
        }
    }
    return num;
}

bool King::isKing()
{
    return true;
}
