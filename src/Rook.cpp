//
//  Rook.cpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#include "Rook.hpp"


Rook::Rook(ofVec3f p, bool c) : Piece(p,c)
{
    model.loadModel("Rook.3ds");
}

bool Rook::legal(ofVec3f p)
{
    
    if( (p.x!=position.x && p.y==position.y && p.z==position.z) )//Check rows
    {
        for(int i=0;i<o.size();i++)
        {
            //is there a piece on that axis?
            if( (o[i]->position.x!=position.x && o[i]->position.y==position.y && o[i]->position.z==position.z) )
            {
                //is the piece between the considered square and the rook?
                if( (position.distance(o[i]->position) < position.distance(p)) && ( (o[i]->position.x>position.x && p.x>position.x)  or (o[i]->position.x<position.x && p.x<position.x) ) )
                    return false;
            }
        }
    }
    else if( (p.x==position.x && p.y!=position.y && p.z==position.z) )//Check columns
    {
        for(int i=0;i<o.size();i++)
        {
            if( (o[i]->position.x==position.x && o[i]->position.y!=position.y && o[i]->position.z==position.z) )
            {
                if( position.distance(o[i]->position) < position.distance(p) && ( (o[i]->position.y>position.y && p.y>position.y)  or (o[i]->position.y<position.y && p.y<position.y) )  )
                    return false;
            }
        }
    }
    
    else if( (p.x==position.x && p.y==position.y && p.z!=position.z))//Check Z axis
    {
        for(int i=0;i<o.size();i++)
        {
            if( o[i]->position.x==position.x && o[i]->position.y==position.y && o[i]->position.z!=position.z  )
            {
                if(  (position.distance(o[i]->position) < position.distance(p) ) && ( ( o[i]->position.z>position.z && p.x>position.z ) or (o[i]->position.z<position.z && p.z<position.z) ) )
                    return false;
            }
        }
    }
    
    else
    {
        return false;
    }
    
    
    return true;
}
