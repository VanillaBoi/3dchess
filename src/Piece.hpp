//
//  Piece.hpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#ifndef Piece_hpp
#define Piece_hpp

#include "ofxAssimpModelLoader.h"
#include "of3dGraphics.h"

class Piece{
    
public:
    bool alive=1;
    bool selected=0;
    bool color=0;// 0=black, 1=white
    ofVec3f position;
    ofxAssimpModelLoader model;
    std::vector <ofVec3f> *positions;
    std::vector <Piece*> o;
    
    
    bool move(ofVec3f p);
    void draw();
    virtual bool isKing();
    virtual bool legal(ofVec3f p);//can the piece move to this square?
     virtual int numLegal();
    
    Piece(ofVec3f p,  bool c);
    
};

#endif /* Piece_hpp */
