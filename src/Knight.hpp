//
//  Knight.hpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#ifndef Knight_hpp
#define Knight_hpp

#include "Piece.hpp"

class Knight : public Piece{
    
public:
    Knight(ofVec3f p, bool c);
    bool legal(ofVec3f p);
};


#endif /* Knight_hpp */
