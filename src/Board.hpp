//
//  Board.hpp
//  3DChess
//
//  Created by Marlon Youngerman on 27/12/2018.
//

#ifndef Board_hpp
#define Board_hpp

#include "of3dGraphics.h"
#include "ofGraphics.h"





class Board{
    
public:
    
    Board();
    ofVec3f selected;
    void update();
    void draw();
    void mousePressed();
};


#endif /* Board_hpp */
