//
//  Pawn.cpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#include "Pawn.hpp"

Pawn::Pawn(ofVec3f p, bool c) : Piece(p,c)
{
    model.loadModel("Pawn.3ds");
}

bool Pawn::legal(ofVec3f p)
{
    const int Woffsets[3][3]={
            {0,0,1},
            {0,1,1},
            {0,-1,1} };
    
     const int Boffsets[3][3]={
            {0,0,-1},
            {0,1,-1},
            {0,-1,-1} };
    
    if(color==0)
    {
        for (int i=0;i<3;i++)
        {
            if(p.x == position.x+Woffsets[i][0] && p.y==position.y+Woffsets[i][1] && p.z==position.z+Woffsets[i][2] )
            {
                return true;
            }
        }
    }
    else
    {
        for (int i=0;i<3;i++)
        {
            if(p.x == position.x+Boffsets[i][0] && p.y==position.y+Boffsets[i][1] && p.z==position.z+Boffsets[i][2] )
            {
                return true;
            }
        }
    }
    
    
    return false;
}
